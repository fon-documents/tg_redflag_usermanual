
# Field User
<!-- {docsify-ignore} -->

Download THINK gas mobile application from playstore. Install and open it.

<p>Enter valid field user username and password and click on Sign in.</p>

<p align="center"> <img width="250" height="400" src="media/fu1.jpg"> </p>
  
<div align="center"> 

**`Fig.1` THINK gas mobile application login page**

</div>

After successful login application shows list of projects assigned to field user.

<p align="center"> <img width="250" height="400" src="media/fu2.jpg"> </p>

<div align="center"> 

**`Fig.2` Projects tab**

</div>

On clicking on project or click on download option on the project hover, it will start downloading the data.

<p align="center"> <img width="250" height="400" src="media/fu3.jpg"> </p>

<div align="center"> 

**`Fig.3` Download Project**

</div>

After download is done, application shows message as Download completed.

<p align="center"> <img width="250" height="400" src="media/fu4.jpg"> </p>

<div align="center"> 

**`Fig.4` Project downloaded message**

</div>

## Submit records- Activities

1.	On clicking the project, shows list of activities for steel project.

<p align="center"> <img width="250" height="400" src="media/fu5.jpg"> </p>

<div align="center"> 

**`Fig.5` list of activities of steel project**

</div>

2.	Click on an activity, shows the submitted records of the activity if any.

<p align="center"> <img width="250" height="400" src="media/fu6.jpg"> </p>

<div align="center"> 

**`Fig.6` list of submitted records of an activity**

</div>

3.	Click on + option, opens blank form of the activity with submit and cancel buttons.

<p align="center"> <img width="250" height="400" src="media/fu7.jpg"> </p>

<div align="center"> 

**`Fig.7` New record of an activity**

</div>

4.	Enter the data and click on submit button. Shows confirmation message showing no of flags raised.

<p align="center"> <img width="250" height="400" src="media/fu8.jpg"> </p>

<div align="center"> 

**`Fig.8` sumbit record**

</div>

5.	On clicking on No, application will allow user to continue entering the data.

<p align="center"> <img width="250" height="400" src="media/fu9.jpg"> </p>

<div align="center"> 

**`Fig.9` Red flags raised when field user entered data matches with rule**

</div>

6.	On clicking on yes, record will be submitted with red flags. Shows message as Activity saved successfully.

<p align="center"> <img width="250" height="400" src="media/fu10.jpg"> </p>

<div align="center"> 

**`Fig.10` Submitting record with red flags**

</div>

<p align="center"> <img width="250" height="400" src="media/fu11.jpg"> </p>

<div align="center"> 

**`Fig.11` Record submitted**

</div>

7.	Click on the submitted record, preview will be shown.

<p align="center"> <img width="250" height="400" src="media/fu12.jpg"> </p>

<div align="center"> 

**`Fig.12` Preview of submitted record**

</div>

## Submit records- Assets

1.	Click on assets icon, shows list of submitted records of all the assets.

<p align="center"> <img width="250" height="400" src="media/fu13.jpg"> </p>

<div align="center"> 

**`Fig.13` Activities tab**

</div>

2.	Click on + icon, shows list of assets.

<p align="center"> <img width="250" height="400" src="media/fu14.jpg"> </p>

<div align="center"> 

**`Fig.14` Submitted records of assets**

</div>

<p align="center"> <img width="250" height="400" src="media/fu15.jpg"> </p>

<div align="center"> 

**`Fig.15` list of assets**

</div>

3.	Click on an asset, opens blank form of that asset with submit and cancel buttons.

<p align="center"> <img width="250" height="400" src="media/fu16.jpg"> </p>

<div align="center"> 

**`Fig.16` new record of asset**

</div>

4.	Enter the data and click on submit button.

5.	Shows confirmation message with no. of red flags raised if any.

6.	Click on no, application will allow user to continue entering the data.

<p align="center"> <img width="250" height="400" src="media/fu17.jpg"> </p>

<div align="center"> 

**`Fig.17` Red flags raised when field user entered data matches with rule**

</div>

7.	Click on yes, data of the asset record will be submitted.

<p align="center"> <img width="250" height="400" src="media/fu18.jpg"> </p>

<div align="center"> 

**`Fig.18` submit data with red flags**

</div>

8.	Application show message as Asset saved successfully.

## Red Flag Rule validation

Red flags will be generated when field user submitted data is matching the rules set by super user for fields of assets and activities.

1.	 Text box, Text Area fields:  Only Text matching. Texts are case sensitive.  

Example: If the value entered by Field user is equal to the value configured for the red flag, then textbox should be red flagged. 

2.	Number Field: Value Greater, value Less Than, Value equal to.  

Example: For the temperature field(number) if the range is set from 90 to 100. Then any value entered in the set range should save with a red flag. 

3.	Dropdown, Radio, check box  field: contains, not contains values from the selected items

Example: If Temperature is a Dropdown field having values Rainy, Cloudy, Sunny then condition can be defined as Temperature field contains values [Rainy, Cloudy]. If temperature value is selected as Rainy or Cloudy, then Flag should be assigned. 

4.	Calendar Field: Date Selected is in a given range 

Example: If submission date is a date field having condition to enter date as current date of submission. If the date selected is matching the current date, then it should be red flagged. 

5.	Calculatior field: If the calculated value is in the configure range value 

Example: If calculation field is in configured range, then it should be red flagged.

6.	Blank rule: If a field rule is set as blank, when user has not entered data for that field during submission flag will be raised. 



