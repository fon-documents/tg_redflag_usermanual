import { Injectable } from '@angular/core';
import { ServerService } from '../../server.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  dropdownSettings= {
    singleSelection: true,
    idField: '_id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  userId:string;
  constructor(private services: ServerService, private router: Router) { }
  createUser(url, data) {
    return  this.services.postServersMultipart(url, data);
  }
  getUsers(url) {
    return  this.services.getServers(url, '');
  }
  xlsxtojsonuser(url, data) {
    return  this.services.postServersMultipart(url, data);
  }
  createUserFromFile(url,data){
    return  this.services.postServers(url, data);
  }
  editUrl(data){
    this.router.navigate(['/mainLayout/users/edit', data._id]);
  }
  editUser(url){
    return  this.services.getServers(url, '');
  }
  updateUser(url,data){
    return  this.services.putServersMultipart(url, data);
  }
  deleteUser(url){
    return this.services.deleteServers(url,'');
  }
  getDepartments(url) {
    return  this.services.getServers(url, '');
  }
  getAllActivities(url){
    return this.services.getServers(url,'');
  }
}