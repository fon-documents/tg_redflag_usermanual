
# What is RedFlag?

<!-- > GO DIGITAL FOR DYNAMIC DATA COLLECTION For more info visit https://fieldon.com/. -->

<p> RedFlag is ability to mark a field for its abnormality/ to give special indication to back office team, an option for field engineer to mark a flag on field will be provided.<p>

<p>The functionality comprises of following capabilities</p>

<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->
<!-- [filename](_media/example.js ':include :type=code :fragment=demo') -->
<!-- ![logo](media/Capture.PNG) -->


- Ability to identify a field with capability of “Flag” and defining a condition under which the flag should be
switched on can be defined in administrative application by Super User.

- The following field will have definitions for flag.

    - <code>Textbox</code>
    - <code>Number</code>
    - <code>Dropdown</code>
    - <code>Radio Button</code>
    - <code>Check Box</code>
    - <code>Date</code>	
    - <code>Text area</code>
    - <code>Calculate</code>


- The type of conditions that can be applied will depend on the Data type of the field.

<details>
    <summary>Conditions (Click to expand)</summary>

  - <code>Text field</code> 
   <p> Only Text matching. Texts are case sensitive. <b>Example:</b> If the value entered by FU is not equal to the value configured for the red flag, then textbox will be red flagged </p>

  - <code>Number</code> 
    <p> Value Greater, value Less Than, Value equal to. Example: For the temperature field(number) if the range is set from 90 to 100. Then any value entered above or below the set range shall save with a red flag.</p>

  - <code>Dropdown, Multiple values, or check box</code> 
    <p> If the value selected in Multiple Items or Checkbox is not in the given list then a flag will be raised. For example, if “Temperature” is a Dropdown field having values “Rainy”, “Cloudy”,” Sunny” then condition can be defined as Temperature field not having value in [“Rainy”, “Cloudy”]. If temperature value is selected as Rainy or Cloudy, then Flag will be assigned</p>

 - <code>Date</code> 
 <p> Date Selected is backdate or not within a given range</p>

 - <code>Calculate</code> 
 <p> If the calculated value is not in the range of configure range value then it should submit with a red flag
 </p>

 - <code>Blank</code>
    <p>Auto-fetch from the store, if remains blank (no value) shall submit with a red flag.</p>
</details>

- Field user can view the nos. of red flags generated at the time of submission and cannot edit the mobile
forms after submission

- All web users will be able to view red flags.

- Quality User can turn the flag off during the QA process when it is in his bucket.

- Power user can turn the flag off when assigned to him by QA user.


<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->