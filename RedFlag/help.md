# Help

<p> File Path <code>src\app\pages\users</code> In users.module.ts we have imported these modules. <p>

<details>
<summary>Used Modules & It's Description (Click to expand)</summary>

- <code>NgModule</code> : Supplies configuration metadata.
- <code>CommonModule</code>:	Exports all the basic Angular directives and pipes, RouterModule  For navigation purpose.
- <code>FormsModule, ReactiveFormsModule</code>:	For reactive forms in users create and edit.
- <code>UsersComponent</code>	This component is used for loading the pageheader information
- <code>UsersCreateComponent</code>	This component is used for creating new user
- <code>UsersDeleteComponent</code>	This component is used for deleting user from users list
- <code>UsersEditComponent</code>	This component is used for edit and updating the user details
- <code>UsersBulkimportComponent</code>	This component is used to create multiple uses at a time through excel.
- <code>UsersListComponent</code>	This component is used to show the list of users.
- <code>NgxPaginationModule</code>	For pagination in users list.
- <code>NgMultiSelectDropDownModule</code>	Showing departments  dropdown in users activity.
- <code>TabsModule</code>	For showing userslist tab and activity tab in users.
- <code>BsDatepickerModule</code>	For start date and end date in users activity.
- <code>PageHeaderModule</code>	For page header
- <code>NgSelectModule</code>	For departments dropdown in users edit
- <code>SharingModuleModule</code>	For number directive used for phone number field.
</details>

## Table of contents

<hr> 
<table>
<thead>
<tr>
<th>Source address</th>
<th>Target address</th>
<th>Type</th>
</tr>
</thead>
<tbody><tr>
<td>/&lt;*&gt;.md</td>
<td>/&lt;*&gt;.md</td>
<td>200 (Rewrite)</td>
</tr>
<tr>
<td>/&lt;*&gt;</td>
<td>/index.html</td>
<td>200 (Rewrite)</td>
</tr>
</tbody></table>
<hr> 


## Doc helper

docsify extends Markdown syntax to make your documents more readable.

> Note: For the special code syntax cases, you'd better put them within a code backticks to avoid any conflicting from configurations or emojis. 


### To mark important content

<code>
    To mark important content use this syntax : !> **Time** is money, my friend! 
</code>

is rendered as:

!> **Time** is money, my friend!



<hr>

Powered by [Magikminds](https://www.magikminds.com/ "Visit our Website for more info")

