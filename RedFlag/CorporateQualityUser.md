
# Corporate Quality User
<!-- {docsify-ignore} -->
<p>Enter valid corporate quality user username and password and click on Sign in.</p>

Login as [Corporate Quality User](https://qcm-uat.think-gas.com/redflag/#/login "to view raised RedFlags ")

![](media/cqu.PNG)

<div align="center"> 

**`Fig.1` THINK gas login page**

</div>

- After login we can see the projects page, list of below options in the menu bar.

 - <code>Projects</code>	
 - <code>Store</code>

<hr>

## View Redflags- Assets

1.  Click on map view of a project, opens project activities page showing assets and activities options.

![](media/cqu1.PNG)

<div align="center"> 

**`Fig.2` list of Projects**

</div>

2.  Click on assets, shows list of assets with count of submitted records of it based on the type of project i.e.  steel/MDPE.

![](media/qu4.PNG)

<div align="center"> 

**`Fig.3` list of Assets**

</div>

3.	On clicking on the asset which has submitted records count, shows the records of the asset. Red flagged records will be shown with red flag.

![](media/qu8.PNG)

<div align="center"> 

**`Fig.4` list of Submitted records of assets**

</div>

4.	Click on rejected red flagged record, shows preview with red flagged fields.

![](media/qu14.PNG)

<div align="center"> 

**`Fig.5` preview of rejected red flagged  records**

</div>

5. Click on approved red flagged record, shows preview with red flagged fields.

![](media/cqu2.PNG)

<div align="center"> 

**`Fig.6` preview of approved red flagged  records**

</div>

## Turn Off Red Flags- Assets

1.	Click on pending red flagged record, shows preview of the record with red flagged fields.

![](media/cqu3.PNG)

<div align="center"> 

**`Fig.7` preview of pending red flagged  records**

</div>

2.	Click on un flag option, shows confirmation action message. 

![](media/cqu4.PNG)

<div align="center"> 

**`Fig.8` unflag**

</div>

3.	On Clicking yes, flag for that field will be removed and shows toast message as Flag removed successfully.

![](media/cqu5.PNG)

<div align="center"> 

**`Fig.9` flag removed**

</div>

## Filter Flagged records- Assets

1.	Enter flag in the search bar and click on enter, shows only flagged records of the asset/activity. 

![](media/filter.PNG)

<div align="center"> 

**`Fig.10` filter flagged records**

</div>

2. shows only flagged records of the asset/activity. 

![](media/flag.PNG)

<div align="center"> 

**`Fig.11` list of flagged records**

</div>

# View Red Flags- Activities

1.	Click on activities, shows list of activities with count of submitted records for steel projects.

![](media/qu5.PNG)

<div align="center"> 

**`Fig.12` list of activities**

</div>

2.	Click on activity which has submitted records, shows list of submitted records of that activity. Shows red flags for red flagged records.

![](media/qu6.PNG)

<div align="center"> 

**`Fig.13` list of submitted records of activity**

</div>

Note: For activities Turning off red flags, filtering flagged records process is same as assets.
