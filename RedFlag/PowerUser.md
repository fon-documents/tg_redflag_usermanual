
# Power User
<!-- {docsify-ignore} -->
<p>Enter valid power user username and password and click on Sign in.</p>

Login as [Power User](https://qcm-uat.think-gas.com/redflag/#/login "to view raised RedFlags ")

![](media/pu.PNG)

<div align="center"> 

**`Fig.1` THINK gas login page**

</div>

After login we can see the projects page, list of below options in the menu bar.

 - <code>Projects</code>
 - <code>Vendors</code>
 - <code>Store</code>
 - <code>Store Users</code>
 - <code>Field Users</code>
 - <code>Documents Upload</code>
 - <code>Device Management</code>
 - <code>Download Templates</code>

![](media/pu1.PNG)

<div align="center"> 

**`Fig.2` list of Projects**

</div>

<hr>

## View Red Flags - Assets

1.	Click on map view of a project, opens project activities page showing assets and activities options.

![](media/qu3.PNG)

<div align="center"> 

**`Fig.3` Map view of Projects**

</div>

2.	Click on assets, shows list of assets with count of submitted records of it based on the type of project i.e.  steel/MDPE

![](media/qu4.PNG)

<div align="center"> 

**`Fig.4` list of Assets**

</div>

3.	On clicking on the asset which has submitted records count, shows the records of the asset. Red flagged records will be shown with red flag.

![](media/qu8.PNG)

<div align="center"> 

**`Fig.5` list of Submitted records of assets**

</div>

4.	Click on pending red flagged record, shows preview with red flagged fields. 

![](media/pu2.PNG)

<div align="center"> 

**`Fig.6` preview of pending red flagged record**

</div>

5.	Click on approved red flagged record, shows preview with red flagged fields.

![](media/pu5.PNG)

<div align="center"> 

**`Fig.7` preview of approved red flagged record**

</div>

## Turn Off Red flags- Assets

1.	Click on rejected red flagged record, shows preview of the record with red flagged fields.

![](media/pu3.PNG)

<div align="center"> 

**`Fig.8` preview of rejected red flagged record**

</div>

2.	Click on un flag option, shows confirmation action message. 

![](media/pu4.PNG)

<div align="center"> 

**`Fig.9` unflag the field**

</div>

3.	On Clicking yes, flag for that field will be removed and shows toast message as Flag removed successfully.

# Filter flagged records

1.	Enter flag in the search bar and click on enter.

![](media/filter.PNG)

<div align="center"> 

**`Fig.10` Filter flagged record**

</div>

2. shows only flagged records of the asset/activity. 

![](media/flag.PNG)

<div align="center"> 

**`Fig.11` List of flagged records**

</div>

# View Red Flags- Activities

1.	Click on activities, shows list of activities with count of submitted records for steel projects.

![](media/qu5.PNG)

<div align="center"> 

**`Fig.12` List of activities**

</div>

2.	Click on activity which has submitted records, shows list of submitted records of that activity. Shows red flags for red flagged records.

![](media/qu6.PNG)

<div align="center"> 

**`Fig.13` List of submitted records of activities**

</div>

Note: For activities Turning off red flags, filtering flagged records process is same as assets.