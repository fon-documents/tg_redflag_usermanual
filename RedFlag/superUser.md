# Super User
<!-- {docsify-ignore} -->
<p>Enter valid super user username and password and click on Sign in.</p>

Login as [Super User](https://qcm-uat.think-gas.com/redflag/#/login "to Define RedFlag conditions")

![](media/su1.png)

<div align="center"> 

**`Fig.1` THINK gas login page**

</div>


- After login we can see the Accounts page, list of below options in the menu bar.

    - <code>Dashboard</code>
    - <code>Accounts</code>
    - <code>Administrators</code>
    - <code>Projects</code>
    - <code>Activities</code>
    - <code>Assets</code>	
    - <code>Field users</code>
    - <code>Documents Upload</code>
    - <code>Download Templates</code>

<hr>

## Activities
<p>On Clicking on Activities icon on the left-hand side menu bar, we can see list of activities available.</p>

![](media/su2.png)

<div align="center"> 

**`Fig.2` Activities**

</div>

### Set Rules

1. To set rules, select the activity and hover on it we can see set rules option.

![](media/su3.png)    

<div align="center"> 

**`Fig.3` Set Rule**

</div>

2. Select the set rules option, red flag rules will be opened with already created rule if any or will show No rules defined message and Add new rule option.

![](media/su4.png)

<div align="center"> 

**`Fig.4` Add New Rule**

</div>

3. Click on Add new rule option, blank row will be added with ‘Field’, ‘Rule’ dropdowns, ‘value’ field and save rule, delete rule options in Actions.

![](media/su5.PNG)

<div align="center"> 

**`Fig.5` Click on Add New Rule button**

</div>

![](media/su6.png)

<div align="center"> 

**`Fig.6` Added blank row**

</div>

### Set rule for Text Box field

<p>Click on Add new rule option, blank row will be added with ‘Field’, ‘Rule’ dropdowns, ‘value’ field and save rule, delete rule options in Actions.</p>

1.	On Selecting Field of type Text Box from Field dropdown, in Rule Dropdown Equal to, Not Equal to, Blank options will be visible.

![](media/su7.PNG)

<div align="center"> 

**`Fig.7` Set Rule for Text box field**

</div>

2.	On selecting Rule as Equal to/ Not Equal to, super admin will be able to enter text in value field.

![](media/su8.PNG)

<div align="center"> 

**`Fig.8` Set Rule for Text box field**

</div>

![](media/su9.PNG)

<div align="center"> 

**`Fig.9` Set Rule for text box field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

![](media/su10.PNG)

<div align="center"> 

**`Fig.10` Set Rule for text box field**

</div>

4.	After choosing field, rule and value click on save rule option

![](media/su11.PNG)

<div align="center"> 

**`Fig.11` Save Rule For Text Box Field**

</div>

5.	Application shows toast message as “Rule saved successfully”.

![](media/su12.png)

<div align="center"> 

**`Fig.12` Saved Rule For Text Box Field**

</div>

6.	 In red flag rules page, Rule will be shown in the list.

### Set rule for Text Area field

1.	On Selecting Field of type Text Area from Field dropdown, in Rule Dropdown Equal to, Not Equal to, Blank options will be visible.

![](media/su13.png)

<div align="center"> 

**`Fig.13` Set Rule For Text Area Field**

</div>

![](media/su14.PNG)

<div align="center"> 

**`Fig.14` Set Rule For Text Area Field**

</div>

2.	On selecting Rule as Equal to/ Not Equal to, super admin will be able to enter text in value field.

![](media/su15.PNG)

<div align="center"> 

**`Fig.15` Save Rule For Text Area Field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

4.	After choosing field, rule and value click on save rule option.

![](media/su16.PNG)

<div align="center"> 

**`Fig.16` Save Rule For Text Area Field**

</div>

5.	Application shows toast message as “Rule saved successfully”.

![](media/su17.png)

<div align="center"> 

**`Fig.17` Saved Rule For Text Area Field**

</div>

6.	 In red flag rules page, Rule will be shown in the list.

<hr>

### Set rule for Number field

1.	On Selecting Field of type Number from Field dropdown, in Rule dropdown less than, greater than, Equal to, Blank options will be visible.

![](media/su18.PNG)

<div align="center"> 

**`Fig.18` Set Rule For Number Field**

</div>

2.	On selecting Rule as Greater than/less than/ Equal to, super admin will be able to enter text in value field.

![](media/su19.PNG)

<div align="center"> 

**`Fig.19` Set Rule For Number Field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

![](media/su20.PNG)

<div align="center"> 

**`Fig.20` Save Rule For Number Field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as “Rule saved successfully”.

![](media/su21.png)

<div align="center"> 

**`Fig.21` Saved Rule For Number Field**

</div>

6.	In red flag rules page, Rule will be shown in the list

### Set rule for Dropdown field

1.	On Selecting Field of type Dropdown from Field dropdown, in Rule dropdown contains, not contains, Blank options will be visible

![](media/su22.PNG)

<div align="center"> 

**`Fig.22` Set Rule For Dropdown Field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected dropdown in value field.

![](media/su23.PNG)

<div align="center"> 

**`Fig.23` Set Rule For Dropdown Field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

![](media/su24.PNG)

<div align="center"> 

**`Fig.24` Save Rule For Dropdown Field**

</div>

4.	After choosing field, rule and value click on save rule option.

5.	Application shows toast message as “Rule saved successfully”.

![](media/su25.png)

<div align="center"> 

**`Fig.25` Saved Rule For Dropdown Field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

### Set rule for Radio field

1.	On Selecting Field of type Radio from Field dropdown, in Rule dropdown contains, not contains, Blank options will be visible.

![](media/su26.PNG)

<div align="center"> 

**`Fig.26` Set Rule For Radio Field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected radio in value field.

3.	On selecting Rule as Blank, value field will be disabled.

![](media/su27.PNG)

<div align="center"> 

**`Fig.27` Set Rule For Radio Field**

</div>

4.	After choosing field, rule and value click on save rule option

5.	Application shows toast message as “Rule saved successfully”.

![](media/su28.png)

<div align="center"> 

**`Fig.28` Saved Rule For Radio Field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

### Set rule for CheckBox field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown contains, Not contains, Blank options will be visible.

![](media/su29.png)

<div align="center"> 

**`Fig.29` Set Rule For check box Field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected radio in value field.

3.	On selecting Rule as Blank, value field will be disabled. 

![](media/su30.png)

<div align="center"> 

**`Fig.30` Set Rule For check box Field**

</div>

4.	After choosing field, rule and value click on save rule option. 

5.	Application shows toast message as “Rule saved successfully”.

![](media/su31.png)

<div align="center"> 

**`Fig.31` Saved Rule For check box Field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

### Set rule for Calendar field

1.	On Selecting Field of type calendar from Field dropdown, in Rule dropdown greater than, less than options will be visible

2.	On selecting Rule as greater than/less than super admin will be able to view and select date in value field.

![](media/su32.PNG)

<div align="center"> 

**`Fig.32` Set Rule For Calender Field**

</div>

3.	After choosing field, rule and value click on save rule option. 

4.	Application shows toast message as “Rule saved successfully”.

5.	In red flag rules page, Rule will be shown in the list.

### Set rule for Calculator field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown Greater than, less than, equal to, Blank options will be visible.

![](media/su33.png)

<div align="center"> 

**`Fig.33` Set Rule For Calculator Field**

</div>

2.	On selecting Rule as greater than/less than/equal to, super admin will be able to view and enter the number in value field.

![](media/su34.png)

<div align="center"> 

**`Fig.34` Save Rule For Calculator Field**

</div>

3.	On selecting Rule as Blank, value field will be disabled. After choosing field, rule and value click on save rule option. 

4.	Application shows toast message as “Rule saved successfully”.

![](media/su35.png)

<div align="center"> 

**`Fig.35` Saved Rule For Calculator Field**

</div>

5.	In red flag rules page, Rule will be shown in the list.

### Edit Rule

1.	All the rules will be in editable mode, make changes by selecting the field, rule and value and click on save rule icon

![](media/su36.PNG)

<div align="center"> 

**`Fig.36` Edit Rule**

</div>

2.	New changes will be saved successfully and shows toast message as “Rule saved successfully”.

![](media/su37.png)

<div align="center"> 

**`Fig.37` Edit Rule**

</div>

### Delete Rule

1.	Click on delete icon, application will show confirmation action message.

![](media/su38.PNG)

<div align="center"> 

**`Fig.38` Delete rule**

</div>

2.	On clicking on yes, rule will be deleted successfully.

![](media/su39.PNG)

<div align="center"> 

**`Fig.39` Delete Rule**

</div>

3.	Shows toast message as “Rule deleted successfully "and rule will be removed from the list.

![](media/su40.png)

<div align="center"> 

**`Fig.40` Rule Deleted**

</div>

<hr>

## Assets
Redflag for Assets.

1.	On Clicking on Assets icon on the left-hand side menu bar, we can see list of available steel and MDPE assets. 

![](media/su41.PNG)

<div align="center"> 

**`Fig.41` List of Assets**

</div>

### Set Rules

1.	To set rules, select the assets and hover on it we can see set rules option.

![](media/su42.png)

<div align="center"> 

**`Fig.42` Set Rule Assets**

</div>

2.	Select the set rules option, red flag rules will be opened with already created rule if any or will show No rules defined message and Add new rule option.

![](media/su43.PNG)

<div align="center"> 

**`Fig.43` Add new rule**

</div>

3.	Click on Add new rule option, blank row will be added with ‘Field’, ‘Rule’ dropdowns, ‘value’ field and save rule, delete rule options in Actions.

![](media/su44.PNG)

<div align="center"> 

**`Fig.44` Add new rule**

</div>

Note: To set rules for fields in assets, edit, delete rules follow the same steps which were mentioned above to set rules, edit, delete rules for activities.


### View Red Flags -Assets

1.	Click on projects icon on the left-hand side menu bar, shows list of available projects created by all power users.

![](media/su45.png)

<div align="center"> 

**`Fig.45` List of projects**

</div>

2.	Click on map view of a project, opens project activities page showing assets and activities options.

![](media/su46.PNG)

<div align="center"> 

**`Fig.46` Map view**

</div>

3.	Now click on assets, shows list of assets with submitted records count and red flag for flagged records. 

![](media/su47.PNG)

<div align="center"> 

**`Fig.47` List of Assets**

</div>

4.	On clicking on red flagged record, shows the fields which were red flagged in the preview.

![](media/su48.PNG)

<div align="center"> 

**`Fig.48` Preview of Redflag Record**

</div>


### View Red Flags -Activities

1.	Now click on assets, shows list of assets with submitted records count and red flag for flagged records.

![](media/su49.PNG)

<div align="center"> 

**`Fig. 49` List of activities** 

</div>

2.	On clicking on red flagged record, shows the fields which were red flagged in the preview.

![](media/su50.PNG)

<div align="center"> 

**`Fig.50` Preview Of Activity**

</div>

